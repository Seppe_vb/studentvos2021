/* To avoid CSS expressions while still supporting IE 7 and IE 6, use this script */
/* The script tag referencing this file must be placed before the ending body tag. */

/* Use conditional comments in order to target IE 7 and older:
	<!--[if lt IE 8]><!-->
	<script src="ie7/ie7.js"></script>
	<!--<![endif]-->
*/

(function() {
	function addIcon(el, entity) {
		var html = el.innerHTML;
		el.innerHTML = '<span style="font-family: \'Vos\'">' + entity + '</span>' + html;
	}
	var icons = {
		'icon-pencil': '&#xe905;',
		'icon-floppy-disk': '&#xe962;',
		'icon-bin': '&#xe9ac;',
		'icon-menu': '&#xe9bd;',
		'icon-plus': '&#xea0a;',
		'icon-minus': '&#xea0b;',
		'icon-cross': '&#xea0f;',
		'icon-arrow-right2': '&#xea3c;',
		'icon-undo2': '&#xe967;',
		'icon-switch': '&#xe9b6;',
		'0': 0
		},
		els = document.getElementsByTagName('*'),
		i, c, el;
	for (i = 0; ; i += 1) {
		el = els[i];
		if(!el) {
			break;
		}
		c = el.className;
		c = c.match(/icon-[^\s'"]+/);
		if (c && icons[c[0]]) {
			addIcon(el, icons[c[0]]);
		}
	}
}());
