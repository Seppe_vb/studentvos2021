<?php
// include('../config/vosadmin.php');
// include('../vendor/threepenny/CRUD.php');
$json = file_get_contents('procedure.json');
$data = json_decode($json, TRUE);
echo '<pre>';
// var_dump($data);
echo '</pre>';
foreach ($data['role'] as $role) {
    $roleId = 3;
    if ($role['code'] == 'LDO') {
        $roleId = 2;
    }
    foreach ($role['procedure'] as $item) {
        $rowProc = array(
            "Code" => $item['code'],
            "Name" => $item['title'],
            "Description" => $item['heading'],
            "RoleId" => $roleId,
            "UpdatedOn" => date('Y-m-d H:i:s'));

        $procId = \Threepenny\CRUD::create('Procedure', $rowProc, 'Name');
        if ($procId > 0) {
            echo "Rij toegevoegd! {$rowProc['Name']} is toegevoegd aan Procedure";
            foreach ($item['step'] as $step) {
                $action = 4;
                $text = '';
                if (isset($step['list'])) {
                    $action = 3;
                    foreach ($step['list'] as $listItem) {
                        $text .= $listItem['title'] . "\n";
                    }
                }
                if (isset($step['action'])) {
                    $action = ($step['action'][0]['code']) == 'YES' ? 2 : 1;
                    $text = $step['action'][0]['action'];
                }
                $rowStep = array(
                    "Order" => isset($item['id']) ? $step['id'] : '1',
                    "Name" => $step['title'],
                    "ActionId" => $action,
                    "ProcedureId" => $procId,
                    "Data" => $text,
                    "UpdatedOn" => date('Y-m-d H:i:s'));
                $stepId = \Threepenny\CRUD::create('Step', $rowStep, 'Name');
                if ($stepId > 0) {
                    echo "Rij toegevoegd! {$rowStep['Name']} is toegevoegd aan Step";
                } else {
                    echo "Oeps er is iets fout gelopen! Kan {$rowStep['Name']} niet toevoegen aan Step";
                    echo \Threepenny\CRUD::getMessage();
                }
            }
        } else {
            echo "Oeps er is iets fout gelopen! Kan {$rowProc['Name']} niet toevoegen aan Procedure";
            echo \Threepenny\CRUD::getMessage();
        }
    }
}
