<?php
/* modernways.be
 * created by 3penny
 * Entreprise de modes et de manières modernes
 * Controller for Vos2 app
 * Created on Tuesday 11th of May 2021 04:31:06 PM
 * FileName: Controllers/FunctionController.php
*/ 
namespace ModernWays\Controllers;
class FunctionController extends \Threepenny\MVC\Controller
{
	public function index()
	{
		if (\Threepenny\Identity::isInRole('Admin')) {
			$model['user-name'] = \Threepenny\Identity::get('Name');
			$model['list'] = \Threepenny\CRUD::readAll('Function', 'Name', 'Name,Id');
			$model['message'] = \Threepenny\CRUD::getMessage();
			return $this->view($model);
		} else {
			$model['message'] = 'Geen toegang. Meld je aan met de juiste gebruikersnaam en paswoord';
			return $this->view($model, 'Views/Vos/LoggingIn.php');
		}
	}
	public function createOne()
	{
		if (\Threepenny\Identity::isInRole('Admin')) {
			$model['user-name'] = \Threepenny\Identity::get('Name');
			$model = array(
				'tableName' => 'Function',
				'error' => 'Geen'
			);
			$data = array(
				"Name" => filter_input(INPUT_POST, 'Function-Name', FILTER_SANITIZE_STRING),
				"UpdatedOn" => filter_input(INPUT_POST, 'Function-UpdatedOnDate', FILTER_SANITIZE_STRING ) . 
					' ' . filter_input(INPUT_POST, 'Function-UpdatedOnTime', FILTER_SANITIZE_STRING )
			);
			$id = \Threepenny\CRUD::create('Function', $data, 'Name', '');
			if ($id > 0) {
				header("Location:/Function/ReadingOne/$id");
			} else {
				$model['message'] = "Oeps er is iets fout gelopen! Kan {$data['Name']} niet toevoegen aan Function";
				$model['error'] = \Threepenny\CRUD::getMessage();
				$model['list'] = \Threepenny\CRUD::readAll('Function', 'Name', 'Name,Id');
				return $this->view($model, 'Views/Function/Index.php');
			}
		} else {
			$model['message'] = 'Geen toegang. Meld je aan met de juiste gebruikersnaam en paswoord';
			return $this->view($model, 'Views/Vos/LoggingIn.php');
		}
	}

	public function creatingOne()
	{
		if (\Threepenny\Identity::isInRole('Admin')) {
			$model['user-name'] = \Threepenny\Identity::get('Name');
			$model['list'] = \Threepenny\CRUD::readAll('Function', 'Name', 'Name,Id');
			$model['message'] = \Threepenny\CRUD::getMessage();
			return $this->view($model);
		} else {
			$model['message'] = 'Geen toegang. Meld je aan met de juiste gebruikersnaam en paswoord';
			return $this->view($model, 'Views/Vos/LoggingIn.php');
		}
	}

	public function deleteOne($id)
	{
		if (\Threepenny\Identity::isInRole('Admin')) {
			$model['user-name'] = \Threepenny\Identity::get('Name');
			if (\Threepenny\CRUD::delete('Function', $id, 'Id')) {
				$model['message'] = "Rij gedeleted in Function";
			} else {
				$model['message'] = "Oeps er is iets fout gelopen! Kan rij niet deleten in Function";
				$model['error'] = \Threepenny\CRUD::getMessage();
			}
			$model['list'] = \Threepenny\CRUD::readAll('Function', 'Name', 'Name,Id');
			$model['message'] = \Threepenny\CRUD::getMessage();
			return $this->view($model, 'Views/Function/Index.php');
		} else {
			$model['message'] = 'Geen toegang. Meld je aan met de juiste gebruikersnaam en paswoord';
			return $this->view($model, 'Views/Vos/LoggingIn.php');
		}
	}

	public function readingOne($id)
	{
		if (\Threepenny\Identity::isInRole('Admin')) {
			$model['user-name'] = \Threepenny\Identity::get('Name');
			$row = \Threepenny\CRUD::readOne('Function', $id);
			if ($row) {
				$model['row'] = $row;

				$model['list'] = \Threepenny\CRUD::readAll('Function', 'Name', 'Name,Id');
				$model['message'] = \Threepenny\CRUD::getMessage();
				return $this->view($model);
			}
			else {
				header("Location:/Function/index");
				return false; // just not to get a warning
			}

		} else {
			$model['message'] = 'Geen toegang. Meld je aan met de juiste gebruikersnaam en paswoord';
			return $this->view($model, 'Views/Vos/LoggingIn.php');
		}
	}

	public function updateOne()
	{
		if (\Threepenny\Identity::isInRole('Admin')) {
			$model['user-name'] = \Threepenny\Identity::get('Name');
			$model = array(
				'tableName' => 'Function',
				'error' => 'Geen'
			);
			$data = array(
				"Name" => filter_input(INPUT_POST, 'Function-Name', FILTER_SANITIZE_STRING),
				"Id" => filter_input(INPUT_POST, 'Function-Id', FILTER_SANITIZE_NUMBER_INT),
				"UpdatedOn" => filter_input(INPUT_POST, 'Function-UpdatedOnDate', FILTER_SANITIZE_STRING ) . 
					' ' . filter_input(INPUT_POST, 'Function-UpdatedOnTime', FILTER_SANITIZE_STRING )
			);
			if (\Threepenny\CRUD::update('Function', $data, 'Name')) {
				$model['message'] = "Rij geüpdated {$data['Name']} in Function";
			} else {
				$model['message'] = "Oeps er is iets fout gelopen! Kan {$data['Name']} niet updaten in Function";
				$model['error'] = \Threepenny\CRUD::getMessage();
			}
			$model['row'] = \Threepenny\CRUD::readOne('Function', $data['Id']);
			$model['list'] = \Threepenny\CRUD::readAll('Function', 'Name', 'Name,Id');
			$model['message'] = \Threepenny\CRUD::getMessage();
			return $this->view($model, 'Views/Function/Index.php');
		} else {
			$model['message'] = 'Geen toegang. Meld je aan met de juiste gebruikersnaam en paswoord';
			return $this->view($model, 'Views/Vos/LoggingIn.php');
		}
	}

	public function updatingOne($id)
	{
		if (\Threepenny\Identity::isInRole('Admin')) {
			$model['user-name'] = \Threepenny\Identity::get('Name');
			$row = \Threepenny\CRUD::readOne('Function', $id);
			if ($row) {
				$model['row'] = $row;
				$model['list'] = \Threepenny\CRUD::readAll('Function', 'Name', 'Name,Id');
				$model['message'] = \Threepenny\CRUD::getMessage();
				return $this->view($model);
			}
			else {
				header("Location:/Function/index");
				return false; // just not to get a warning
			}
		} else {
			$model['message'] = 'Geen toegang. Meld je aan met de juiste gebruikersnaam en paswoord';
			return $this->view($model, 'Views/Vos/LoggingIn.php');
		}
	}

}

