<?php
/* modernways.be
 * created by 3penny
 * Entreprise de modes et de manières modernes
 * Controller for Vos2 app
 * Created on Tuesday 11th of May 2021 04:31:06 PM
 * FileName: Controllers/LogController.php
*/ 
namespace ModernWays\Controllers;
class LogController extends \Threepenny\MVC\Controller
{
	public function index()
	{
		if (\Threepenny\Identity::isInRole('Admin')) {
			$model['user-name'] = \Threepenny\Identity::get('Name');
			$model['list'] = \Threepenny\CRUD::readAll('Log', 'UserName', 'UserName,Role,ProcedureTitle,StepTitle,ActionCode');
			$model['message'] = \Threepenny\CRUD::getMessage();
			return $this->view($model);
		} else {
			$model['message'] = 'Geen toegang. Meld je aan met de juiste gebruikersnaam en paswoord';
			return $this->view($model, 'Views/Vos/LoggingIn.php');
		}
	}
	public function createOne()
	{
		if (\Threepenny\Identity::isInRole('Admin')) {
			$model['user-name'] = \Threepenny\Identity::get('Name');
			$model = array(
				'tableName' => 'Log',
				'error' => 'Geen'
			);
			$data = array(
				"UserName" => filter_input(INPUT_POST, 'Log-UserName', FILTER_SANITIZE_STRING),
				"Email" => filter_input(INPUT_POST, 'Log-Email', FILTER_SANITIZE_STRING),
				"Role" => filter_input(INPUT_POST, 'Log-Role', FILTER_SANITIZE_STRING),
				"ProcedureCode" => filter_input(INPUT_POST, 'Log-ProcedureCode', FILTER_SANITIZE_STRING),
				"ProcedureTitle" => filter_input(INPUT_POST, 'Log-ProcedureTitle', FILTER_SANITIZE_STRING),
				"StepTitle" => filter_input(INPUT_POST, 'Log-StepTitle', FILTER_SANITIZE_STRING),
				"ActionCode" => filter_input(INPUT_POST, 'Log-ActionCode', FILTER_SANITIZE_STRING),
				"CallNumber" => filter_input(INPUT_POST, 'Log-CallNumber', FILTER_SANITIZE_STRING),
				"SendNumber" => filter_input(INPUT_POST, 'Log-SendNumber', FILTER_SANITIZE_STRING)
			);
			$id = \Threepenny\CRUD::create('Log', $data, 'UserName', '');
			if ($id > 0) {
				header("Location:/Log/ReadingOne/$id");
			} else {
				$model['message'] = "Oeps er is iets fout gelopen! Kan {$data['Name']} niet toevoegen aan Log";
				$model['error'] = \Threepenny\CRUD::getMessage();
				$model['list'] = \Threepenny\CRUD::readAll('Log', 'UserName', 'UserName,Role,ProcedureTitle,StepTitle,ActionCode');
				return $this->view($model, 'Views/Log/Index.php');
			}
		} else {
			$model['message'] = 'Geen toegang. Meld je aan met de juiste gebruikersnaam en paswoord';
			return $this->view($model, 'Views/Vos/LoggingIn.php');
		}
	}

	public function creatingOne()
	{
		if (\Threepenny\Identity::isInRole('Admin')) {
			$model['user-name'] = \Threepenny\Identity::get('Name');
			$model['list'] = \Threepenny\CRUD::readAll('Log', 'UserName', 'UserName,Role,ProcedureTitle,StepTitle,ActionCode');
			$model['message'] = \Threepenny\CRUD::getMessage();
			return $this->view($model);
		} else {
			$model['message'] = 'Geen toegang. Meld je aan met de juiste gebruikersnaam en paswoord';
			return $this->view($model, 'Views/Vos/LoggingIn.php');
		}
	}

	public function deleteOne($id)
	{
		if (\Threepenny\Identity::isInRole('Admin')) {
			$model['user-name'] = \Threepenny\Identity::get('Name');
			if (\Threepenny\CRUD::delete('Log', $id, 'Id')) {
				$model['message'] = "Rij gedeleted in Log";
			} else {
				$model['message'] = "Oeps er is iets fout gelopen! Kan rij niet deleten in Log";
				$model['error'] = \Threepenny\CRUD::getMessage();
			}
			$model['list'] = \Threepenny\CRUD::readAll('Log', 'UserName', 'UserName,Role,ProcedureTitle,StepTitle,ActionCode');
			$model['message'] = \Threepenny\CRUD::getMessage();
			return $this->view($model, 'Views/Log/Index.php');
		} else {
			$model['message'] = 'Geen toegang. Meld je aan met de juiste gebruikersnaam en paswoord';
			return $this->view($model, 'Views/Vos/LoggingIn.php');
		}
	}

	public function readingOne($id)
	{
		if (\Threepenny\Identity::isInRole('Admin')) {
			$model['user-name'] = \Threepenny\Identity::get('Name');
			$row = \Threepenny\CRUD::readOne('Log', $id);
			if ($row) {
				$model['row'] = $row;
				$model['list'] = \Threepenny\CRUD::readAll('Log', 'UserName', 'UserName,Role,ProcedureTitle,StepTitle,ActionCode');
				$model['message'] = \Threepenny\CRUD::getMessage();
				return $this->view($model);
			}
			else {
				header("Location:/Log/index");
				return false; // just not to get a warning
			}
		} else {
			$model['message'] = 'Geen toegang. Meld je aan met de juiste gebruikersnaam en paswoord';
			return $this->view($model, 'Views/Vos/LoggingIn.php');
		}
	}

	public function updateOne()
	{
		if (\Threepenny\Identity::isInRole('Admin')) {
			$model['user-name'] = \Threepenny\Identity::get('Name');
			$model = array(
				'tableName' => 'Log',
				'error' => 'Geen'
			);
			$data = array(
				"UserName" => filter_input(INPUT_POST, 'Log-UserName', FILTER_SANITIZE_STRING),
				"Email" => filter_input(INPUT_POST, 'Log-Email', FILTER_SANITIZE_STRING),
				"Role" => filter_input(INPUT_POST, 'Log-Role', FILTER_SANITIZE_STRING),
				"ProcedureCode" => filter_input(INPUT_POST, 'Log-ProcedureCode', FILTER_SANITIZE_STRING),
				"ProcedureTitle" => filter_input(INPUT_POST, 'Log-ProcedureTitle', FILTER_SANITIZE_STRING),
				"StepTitle" => filter_input(INPUT_POST, 'Log-StepTitle', FILTER_SANITIZE_STRING),
				"ActionCode" => filter_input(INPUT_POST, 'Log-ActionCode', FILTER_SANITIZE_STRING),
				"CallNumber" => filter_input(INPUT_POST, 'Log-CallNumber', FILTER_SANITIZE_STRING),
				"SendNumber" => filter_input(INPUT_POST, 'Log-SendNumber', FILTER_SANITIZE_STRING),
				"Id" => filter_input(INPUT_POST, 'Log-Id', FILTER_SANITIZE_NUMBER_INT)
			);
			if (\Threepenny\CRUD::update('Log', $data, 'UserName')) {
				$model['message'] = "Rij geüpdated {$data['UserName']} in Log";
			} else {
				$model['message'] = "Oeps er is iets fout gelopen! Kan {$data['UserName']} niet updaten in Log";
				$model['error'] = \Threepenny\CRUD::getMessage();
			}
			$model['row'] = \Threepenny\CRUD::readOne('Log', $data['Id']);
			$model['list'] = \Threepenny\CRUD::readAll('Log', 'UserName', 'UserName,Role,ProcedureTitle,StepTitle,ActionCode');
			$model['message'] = \Threepenny\CRUD::getMessage();
			return $this->view($model, 'Views/Log/Index.php');
		} else {
			$model['message'] = 'Geen toegang. Meld je aan met de juiste gebruikersnaam en paswoord';
			return $this->view($model, 'Views/Vos/LoggingIn.php');
		}
	}

	public function updatingOne($id)
	{
		if (\Threepenny\Identity::isInRole('Admin')) {
			$model['user-name'] = \Threepenny\Identity::get('Name');
			$row = \Threepenny\CRUD::readOne('Log', $id);
			if ($row) {
				$model['row'] = $row;
				$model['list'] = \Threepenny\CRUD::readAll('Log', 'UserName', 'UserName,Role,ProcedureTitle,StepTitle,ActionCode');
				$model['message'] = \Threepenny\CRUD::getMessage();
				return $this->view($model);
			}
			else {
				header("Location:/Log/index");
				return false; // just not to get a warning
			}
		} else {
			$model['message'] = 'Geen toegang. Meld je aan met de juiste gebruikersnaam en paswoord';
			return $this->view($model, 'Views/Vos/LoggingIn.php');
		}
	}

}

