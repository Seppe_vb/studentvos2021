<?php
/* modernways.be
 * created by 3penny
 * Entreprise de modes et de manières modernes
 * Controller for Vos2 app
 * Created on Tuesday 11th of May 2021 04:31:06 PM
 * FileName: Controllers/StepController.php
*/
namespace ModernWays\Controllers;
class StepController extends \Threepenny\MVC\Controller
{
    public function index()
    {
        if (\Threepenny\Identity::isInRole('Admin')) {
            $model['user-name'] = \Threepenny\Identity::get('Name');
            $model['list'] = \Threepenny\CRUD::readAll('Step', 'ProcedureIdCode, ProcedureIdName', 'Name,Procedure.Code,Procedure.Name,Order');
            $model['message'] = \Threepenny\CRUD::getMessage();
            return $this->view($model);
        } else {
            $model['message'] = 'Geen toegang. Meld je aan met de juiste gebruikersnaam en paswoord';
            return $this->view($model, 'Views/Vos/LoggingIn.php');
        }
    }
    public function createOne()
    {
        if (\Threepenny\Identity::isInRole('Admin')) {
            $model['user-name'] = \Threepenny\Identity::get('Name');
            $model = array(
                'tableName' => 'Step',
                'error' => 'Geen'
            );
            $data = array(
                "Name" => filter_input(INPUT_POST, 'Step-Name', FILTER_SANITIZE_STRING),
                "Description" => filter_input(INPUT_POST, 'Step-Description', FILTER_SANITIZE_STRING),
                "ActionId" => filter_input(INPUT_POST, 'Step-ActionId', FILTER_SANITIZE_NUMBER_INT),
                "ProcedureId" => filter_input(INPUT_POST, 'Step-ProcedureId', FILTER_SANITIZE_NUMBER_INT),
                "Order" => filter_input(INPUT_POST, 'Step-Order', FILTER_SANITIZE_NUMBER_INT),
                "Data" => filter_input(INPUT_POST, 'Step-Data', FILTER_SANITIZE_STRING),
                "UpdatedOn" => filter_input(INPUT_POST, 'Step-UpdatedOnDate', FILTER_SANITIZE_STRING ) .
                    ' ' . filter_input(INPUT_POST, 'Step-UpdatedOnTime', FILTER_SANITIZE_STRING )
            );
            $id = \Threepenny\CRUD::create('Step', $data, 'ProcedureIdCode', '');
            if ($id > 0) {
                header("Location:/Step/ReadingOne/$id");
            } else {
                $model['message'] = "Oeps er is iets fout gelopen! Kan {$data['Name']} niet toevoegen aan Step";
                $model['error'] = \Threepenny\CRUD::getMessage();
                $model['list'] = \Threepenny\CRUD::readAll('Step', 'ProcedureIdCode, ProcedureIdName', 'Name,Procedure.Code,Procedure.Name,Order');
                return $this->view($model, 'Views/Step/Index.php');
            }
        } else {
            $model['message'] = 'Geen toegang. Meld je aan met de juiste gebruikersnaam en paswoord';
            return $this->view($model, 'Views/Vos/LoggingIn.php');
        }
    }

    public function creatingOne()
    {
        if (\Threepenny\Identity::isInRole('Admin')) {
            $model['user-name'] = \Threepenny\Identity::get('Name');
            $model['list'] = \Threepenny\CRUD::readAll('Step', 'ProcedureIdCode, ProcedureIdName', 'Name,Procedure.Code,Procedure.Name,Order');
            $model['message'] = \Threepenny\CRUD::getMessage();
            $model['ActionList'] =
                \Threepenny\CRUD::readAll('Action', 'Code,Name', 'Code,Name,Id');
            $model['ProcedureList'] =
                \Threepenny\CRUD::readAll('Procedure', 'Code,Name', 'Code,Name,Id');
            return $this->view($model);
        } else {
            $model['message'] = 'Geen toegang. Meld je aan met de juiste gebruikersnaam en paswoord';
            return $this->view($model, 'Views/Vos/LoggingIn.php');
        }
    }

    public function deleteOne($id)
    {
        if (\Threepenny\Identity::isInRole('Admin')) {
            $model['user-name'] = \Threepenny\Identity::get('Name');
            if (\Threepenny\CRUD::delete('Step', $id, 'Id')) {
                $model['message'] = "Rij gedeleted in Step";
            } else {
                $model['message'] = "Oeps er is iets fout gelopen! Kan rij niet deleten in Step";
                $model['error'] = \Threepenny\CRUD::getMessage();
            }
            $model['list'] = \Threepenny\CRUD::readAll('Step', 'ProcedureIdCode, ProcedureIdName', 'Name,Procedure.Code,Procedure.Name,Order');
            $model['message'] = \Threepenny\CRUD::getMessage();
            return $this->view($model, 'Views/Step/Index.php');
        } else {
            $model['message'] = 'Geen toegang. Meld je aan met de juiste gebruikersnaam en paswoord';
            return $this->view($model, 'Views/Vos/LoggingIn.php');
        }
    }

    public function readingOne($id)
    {
        if (\Threepenny\Identity::isInRole('Admin')) {
            $model['user-name'] = \Threepenny\Identity::get('Name');
            $row = \Threepenny\CRUD::readOne('Step', $id);
            if ($row) {
                $model['row'] = $row;
                $model['list'] = \Threepenny\CRUD::readAll('Step', 'ProcedureIdCode, ProcedureIdName', 'Name,Procedure.Code,Procedure.Name,Order');
                $model['message'] = \Threepenny\CRUD::getMessage();
                $model['ActionList'] =
                    \Threepenny\CRUD::readAll('Action', 'Code,Name', 'Code,Name,Id');
                $model['ProcedureList'] =
                    \Threepenny\CRUD::readAll('Procedure', 'Code,Name', 'Code,Name,Id');
                return $this->view($model);
            }
            else {
                header("Location:/Step/index");
                return false; // just not to get a warning
            }
        } else {
            $model['message'] = 'Geen toegang. Meld je aan met de juiste gebruikersnaam en paswoord';
            return $this->view($model, 'Views/Vos/LoggingIn.php');
        }
    }

    public function updateOne()
    {
        if (\Threepenny\Identity::isInRole('Admin')) {
            $model['user-name'] = \Threepenny\Identity::get('Name');
            $model = array(
                'tableName' => 'Step',
                'error' => 'Geen'
            );
            $data = array(
                "Name" => filter_input(INPUT_POST, 'Step-Name', FILTER_SANITIZE_STRING),
                "Description" => filter_input(INPUT_POST, 'Step-Description', FILTER_SANITIZE_STRING),
                "ActionId" => filter_input(INPUT_POST, 'Step-ActionId', FILTER_SANITIZE_NUMBER_INT),
                "ProcedureId" => filter_input(INPUT_POST, 'Step-ProcedureId', FILTER_SANITIZE_NUMBER_INT),
                "Order" => filter_input(INPUT_POST, 'Step-Order', FILTER_SANITIZE_NUMBER_INT),
                "Data" => filter_input(INPUT_POST, 'Step-Data', FILTER_SANITIZE_STRING),
                "Id" => filter_input(INPUT_POST, 'Step-Id', FILTER_SANITIZE_NUMBER_INT),
                "UpdatedOn" => filter_input(INPUT_POST, 'Step-UpdatedOnDate', FILTER_SANITIZE_STRING ) .
                    ' ' . filter_input(INPUT_POST, 'Step-UpdatedOnTime', FILTER_SANITIZE_STRING )
            );
            // $data['Name'] ipv $data[ProcedireId]

            if (\Threepenny\CRUD::update('Step', $data, 'Name')) {
                $model['message'] = "Rij geüpdated {$data['Name']} in Step";
            } else {
                $model['message'] = "Oeps er is iets fout gelopen! Kan {$data['Name']} niet updaten in Step";
                $model['error'] = \Threepenny\CRUD::getMessage();
            }
            $model['row'] = \Threepenny\CRUD::readOne('Step', $data['Id']);
            $model['list'] = \Threepenny\CRUD::readAll('Step', 'ProcedureIdCode, ProcedureIdName', 'Name,Procedure.Code,Procedure.Name,Order');
            $model['message'] = \Threepenny\CRUD::getMessage();
            $model['ActionList'] =
                \Threepenny\CRUD::readAll('Action', 'Code,Name', 'Code,Name,Id');
            $model['ProcedureList'] =
                \Threepenny\CRUD::readAll('Procedure', 'Code,Name', 'Code,Name,Id');
            return $this->view($model, 'Views/Step/Index.php');
        } else {
            $model['message'] = 'Geen toegang. Meld je aan met de juiste gebruikersnaam en paswoord';
            return $this->view($model, 'Views/Vos/LoggingIn.php');
        }
    }

    public function updatingOne($id)
    {
        if (\Threepenny\Identity::isInRole('Admin')) {
            $model['user-name'] = \Threepenny\Identity::get('Name');
            $row = \Threepenny\CRUD::readOne('Step', $id);
            if ($row) {
                $model['row'] = $row;
                $model['list'] = \Threepenny\CRUD::readAll('Step', 'ProcedureIdCode, ProcedureIdName', 'Name,Procedure.Code,Procedure.Name,Order');
                $model['message'] = \Threepenny\CRUD::getMessage();
                $model['ActionList'] =
                    \Threepenny\CRUD::readAll('Action', 'Code,Name', 'Code,Name,Id');
                $model['ProcedureList'] =
                    \Threepenny\CRUD::readAll('Procedure', 'Code,Name', 'Code,Name,Id');
                return $this->view($model);
            }
            else {
                header("Location:/Step/index");
                return false; // just not to get a warning
            }
        } else {
            $model['message'] = 'Geen toegang. Meld je aan met de juiste gebruikersnaam en paswoord';
            return $this->view($model, 'Views/Vos/LoggingIn.php');
        }
    }

}

