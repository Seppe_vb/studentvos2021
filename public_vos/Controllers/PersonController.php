<?php
/* modernways.be
 * created by 3penny
 * Entreprise de modes et de manières modernes
 * Controller for Vos2 app
 * Created on Tuesday 11th of May 2021 04:31:06 PM
 * FileName: Controllers/PersonController.php
*/ 
namespace ModernWays\Controllers;
class PersonController extends \Threepenny\MVC\Controller
{
	public function index()
	{
		if (\Threepenny\Identity::isInRole('Admin')) {
			$model['user-name'] = \Threepenny\Identity::get('Name');
			$model['list'] = \Threepenny\CRUD::readAll('Person', 'LastName, FirstName', 'FirstName,LastName,Id');
			$model['message'] = \Threepenny\CRUD::getMessage();
			return $this->view($model);
		} else {
			$model['message'] = 'Geen toegang. Meld je aan met de juiste gebruikersnaam en paswoord';
			return $this->view($model, 'Views/Vos/LoggingIn.php');
		}
	}
	public function createOne()
	{
		if (\Threepenny\Identity::isInRole('Admin')) {
			$model['user-name'] = \Threepenny\Identity::get('Name');
			$model = array(
				'tableName' => 'Person',
				'error' => 'Geen'
			);
			$data = array(
				"FirstName" => filter_input(INPUT_POST, 'Person-FirstName', FILTER_SANITIZE_STRING),
				"LastName" => filter_input(INPUT_POST, 'Person-LastName', FILTER_SANITIZE_STRING),
				"Mobile" => filter_input(INPUT_POST, 'Person-Mobile', FILTER_SANITIZE_STRING),
				"Email" => filter_input(INPUT_POST, 'Person-Email', FILTER_SANITIZE_STRING),
				"Street" => filter_input(INPUT_POST, 'Person-Street', FILTER_SANITIZE_STRING),
				"PostalCode" => filter_input(INPUT_POST, 'Person-PostalCode', FILTER_SANITIZE_STRING),
				"City" => filter_input(INPUT_POST, 'Person-City', FILTER_SANITIZE_STRING),
				"UpdatedOn" => filter_input(INPUT_POST, 'Person-UpdatedOnDate', FILTER_SANITIZE_STRING ) . 
					' ' . filter_input(INPUT_POST, 'Person-UpdatedOnTime', FILTER_SANITIZE_STRING )
			);
			$id = \Threepenny\CRUD::create('Person', $data, 'LastName', '');
			if ($id > 0) {
				header("Location:/Person/ReadingOne/$id");
			} else {
				$model['message'] = "Oeps er is iets fout gelopen! Kan {$data['Name']} niet toevoegen aan Person";
				$model['error'] = \Threepenny\CRUD::getMessage();
				$model['list'] = \Threepenny\CRUD::readAll('Person', 'LastName, FirstName', 'FirstName,LastName,Id');
				return $this->view($model, 'Views/Person/Index.php');
			}
		} else {
			$model['message'] = 'Geen toegang. Meld je aan met de juiste gebruikersnaam en paswoord';
			return $this->view($model, 'Views/Vos/LoggingIn.php');
		}
	}

	public function creatingOne()
	{
		if (\Threepenny\Identity::isInRole('Admin')) {
			$model['user-name'] = \Threepenny\Identity::get('Name');
			$model['list'] = \Threepenny\CRUD::readAll('Person', 'LastName, FirstName', 'FirstName,LastName,Id');
			$model['message'] = \Threepenny\CRUD::getMessage();
			return $this->view($model);
		} else {
			$model['message'] = 'Geen toegang. Meld je aan met de juiste gebruikersnaam en paswoord';
			return $this->view($model, 'Views/Vos/LoggingIn.php');
		}
	}

	public function deleteOne($id)
	{
		if (\Threepenny\Identity::isInRole('Admin')) {
			$model['user-name'] = \Threepenny\Identity::get('Name');
			if (\Threepenny\CRUD::delete('Person', $id, 'Id')) {
				$model['message'] = "Rij gedeleted in Person";
			} else {
				$model['message'] = "Oeps er is iets fout gelopen! Kan rij niet deleten in Person";
				$model['error'] = \Threepenny\CRUD::getMessage();
			}
			$model['list'] = \Threepenny\CRUD::readAll('Person', 'LastName, FirstName', 'FirstName,LastName,Id');
			$model['message'] = \Threepenny\CRUD::getMessage();
			return $this->view($model, 'Views/Person/Index.php');
		} else {
			$model['message'] = 'Geen toegang. Meld je aan met de juiste gebruikersnaam en paswoord';
			return $this->view($model, 'Views/Vos/LoggingIn.php');
		}
	}

	public function readingOne($id)
	{
		if (\Threepenny\Identity::isInRole('Admin')) {
			$model['user-name'] = \Threepenny\Identity::get('Name');
			$row = \Threepenny\CRUD::readOne('Person', $id);
			if ($row) {
				$model['row'] = $row;
				$model['list'] = \Threepenny\CRUD::readAll('Person', 'LastName, FirstName', 'FirstName,LastName,Id');
				$model['message'] = \Threepenny\CRUD::getMessage();
				return $this->view($model);
			}
			else {
				header("Location:/Person/index");
				return false; // just not to get a warning
			}
		} else {
			$model['message'] = 'Geen toegang. Meld je aan met de juiste gebruikersnaam en paswoord';
			return $this->view($model, 'Views/Vos/LoggingIn.php');
		}
	}

	public function updateOne()
	{
		if (\Threepenny\Identity::isInRole('Admin')) {
			$model['user-name'] = \Threepenny\Identity::get('Name');
			$model = array(
				'tableName' => 'Person',
				'error' => 'Geen'
			);
			$data = array(
				"FirstName" => filter_input(INPUT_POST, 'Person-FirstName', FILTER_SANITIZE_STRING),
				"LastName" => filter_input(INPUT_POST, 'Person-LastName', FILTER_SANITIZE_STRING),
				"Mobile" => filter_input(INPUT_POST, 'Person-Mobile', FILTER_SANITIZE_STRING),
				"Email" => filter_input(INPUT_POST, 'Person-Email', FILTER_SANITIZE_STRING),
				"Street" => filter_input(INPUT_POST, 'Person-Street', FILTER_SANITIZE_STRING),
				"PostalCode" => filter_input(INPUT_POST, 'Person-PostalCode', FILTER_SANITIZE_STRING),
				"City" => filter_input(INPUT_POST, 'Person-City', FILTER_SANITIZE_STRING),
				"Id" => filter_input(INPUT_POST, 'Person-Id', FILTER_SANITIZE_NUMBER_INT),
				"UpdatedOn" => filter_input(INPUT_POST, 'Person-UpdatedOnDate', FILTER_SANITIZE_STRING ) . 
					' ' . filter_input(INPUT_POST, 'Person-UpdatedOnTime', FILTER_SANITIZE_STRING )
			);
			if (\Threepenny\CRUD::update('Person', $data, 'LastName')) {
				$model['message'] = "Rij geüpdated {$data['LastName']} in Person";
			} else {
				$model['message'] = "Oeps er is iets fout gelopen! Kan {$data['LastName']} niet updaten in Person";
				$model['error'] = \Threepenny\CRUD::getMessage();
			}
			$model['row'] = \Threepenny\CRUD::readOne('Person', $data['Id']);
			$model['list'] = \Threepenny\CRUD::readAll('Person', 'LastName, FirstName', 'FirstName,LastName,Id');
			$model['message'] = \Threepenny\CRUD::getMessage();
			return $this->view($model, 'Views/Person/Index.php');
		} else {
			$model['message'] = 'Geen toegang. Meld je aan met de juiste gebruikersnaam en paswoord';
			return $this->view($model, 'Views/Vos/LoggingIn.php');
		}
	}

	public function updatingOne($id)
	{
		if (\Threepenny\Identity::isInRole('Admin')) {
			$model['user-name'] = \Threepenny\Identity::get('Name');
			$row = \Threepenny\CRUD::readOne('Person', $id);
			if ($row) {
				$model['row'] = $row;
				$model['list'] = \Threepenny\CRUD::readAll('Person', 'LastName, FirstName', 'FirstName,LastName,Id');
				$model['message'] = \Threepenny\CRUD::getMessage();
				return $this->view($model);
			}
			else {
				header("Location:/Person/index");
				return false; // just not to get a warning
			}
		} else {
			$model['message'] = 'Geen toegang. Meld je aan met de juiste gebruikersnaam en paswoord';
			return $this->view($model, 'Views/Vos/LoggingIn.php');
		}
	}

}

