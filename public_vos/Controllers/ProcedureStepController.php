<?php


namespace ModernWays\Controllers;


class ProcedureStepController extends \Threepenny\MVC\Controller
{
    public function index($procedureId)
    {
        if (\Threepenny\Identity::isInRole('Admin')) {
            $model['user-name'] = \Threepenny\Identity::get('Name');
            $rowProcedure = \Threepenny\CRUD::readOne('Procedure', $procedureId);
            $stepColumnsToShow = array('Name', 'Id', 'Order', 'ProcedureId');
            if ($rowProcedure) {
                $model['rowProcedure'] = $rowProcedure;
                $model['listStep'] = \Threepenny\CRUD::readAllWhere('Step', $procedureId,
                    'ProcedureId', 'Order, Name', $stepColumnsToShow);
                $model['message'] = \Threepenny\CRUD::getMessage();
                $model['roleDetail'] = \Threepenny\CRUD::getRole(null, $procedureId);
                return $this->view($model);
            } else {
                header("Location:/Procedure/index");
                return false; // just not to get a warning
            }
        } else {
            $model['message'] = 'Geen toegang. Meld je aan met de juiste gebruikersnaam en paswoord';
            return $this->view($model, 'Views/Vos/LoggingIn.php');
        }
    }

    public function createOne()
    {
        if (\Threepenny\Identity::isInRole('Admin')) {
            $model['user-name'] = \Threepenny\Identity::get('Name');
            $model = array(
                'tableName' => 'Step',
                'error' => 'Geen'
            );
            $data = array(
                "Name" => filter_input(INPUT_POST, 'Step-Name', FILTER_SANITIZE_STRING),
                "Description" => filter_input(INPUT_POST, 'Step-Description', FILTER_SANITIZE_STRING),
                "ActionId" => filter_input(INPUT_POST, 'Step-ActionId', FILTER_SANITIZE_NUMBER_INT),
                "ProcedureId" => filter_input(INPUT_POST, 'Step-ProcedureId', FILTER_SANITIZE_NUMBER_INT),
                "Order" => filter_input(INPUT_POST, 'Step-Order', FILTER_SANITIZE_NUMBER_INT),
                "Data" => filter_input(INPUT_POST, 'Step-Data', FILTER_SANITIZE_STRING),
                "UpdatedOn" => filter_input(INPUT_POST, 'Step-UpdatedOnDate', FILTER_SANITIZE_STRING) .
                    ' ' . filter_input(INPUT_POST, 'Step-UpdatedOnTime', FILTER_SANITIZE_STRING)
            );
            $id = \Threepenny\CRUD::create('Step', $data, 'ProcedureIdCode', '');
            if ($id > 0) {
                header("Location:/ProcedureStep/index/{$data['ProcedureId']}");
            } else {
                $model['message'] = "Oeps er is iets fout gelopen! Kan {$data['Name']} niet toevoegen aan Step";
                $model['error'] = \Threepenny\CRUD::getMessage();
                header("Location:/ProcedureStep/index/{$data['ProcedureId']}");
                // return $this->view($model, 'Views/Step/Index.php');
            }
        } else {
            $model['message'] = 'Geen toegang. Meld je aan met de juiste gebruikersnaam en paswoord';
            return $this->view($model, 'Views/Vos/LoggingIn.php');
        }
    }

    public function creatingOne($procedureId)
    {
        if (\Threepenny\Identity::isInRole('Admin')) {
            $stepColumnsToShow = array('Name', 'Id', 'Order', 'ProcedureId');
            $model['rowProcedure'] = \Threepenny\CRUD::readOne('Procedure', $procedureId);
            $model['user-name'] = \Threepenny\Identity::get('Name');
            $model['listStep'] = \Threepenny\CRUD::readAllWhere('Step', $procedureId,
                'ProcedureId', 'Order, Name', $stepColumnsToShow);
            $model['message'] = \Threepenny\CRUD::getMessage();
            $model['ActionList'] = \Threepenny\CRUD::readAll('Action',
                'Code,Name', 'Code,Name,Id');
            $model['roleDetail'] = \Threepenny\CRUD::getRole(null, $procedureId);
            return $this->view($model);
        } else {
            $model['message'] = 'Geen toegang. Meld je aan met de juiste gebruikersnaam en paswoord';
            return $this->view($model, 'Views/Vos/LoggingIn.php');
        }
    }

    public function deleteOne($stepId)
    {
        if (\Threepenny\Identity::isInRole('Admin')) {
            $model['user-name'] = \Threepenny\Identity::get('Name');
            $rowStep = \Threepenny\CRUD::readOne('Step', $stepId);

            if (\Threepenny\CRUD::delete('Step', $stepId, 'Id')) {
                $model['message'] = "Rij gedeleted in Step";
            } else {
                $model['message'] = "Oeps er is iets fout gelopen! Kan rij niet deleten in Step";
                $model['error'] = \Threepenny\CRUD::getMessage();
            }
            header("Location:/ProcedureStep/Index/{$rowStep['ProcedureId']}");
            //  return $this->view($model, 'Views/ProcedureStep/Index/.php');
        } else {
            $model['message'] = 'Geen toegang. Meld je aan met de juiste gebruikersnaam en paswoord';
            return $this->view($model, 'Views/Vos/LoggingIn.php');
        }
    }

    public function readingOne($stepId)
    {
        if (\Threepenny\Identity::isInRole('Admin')) {
            $model['user-name'] = \Threepenny\Identity::get('Name');
            $rowStep = \Threepenny\CRUD::readOne('Step', $stepId);
            $stepColumnsToShow = array('Name', 'Id', 'Order', 'ProcedureId');
            if ($rowStep) {
                $model['rowStep'] = $rowStep;
                $model['listStep'] = \Threepenny\CRUD::readAllWhere('Step', $rowStep['ProcedureId'],
                    'ProcedureId', 'Order, Name', $stepColumnsToShow);
                $model['message'] = \Threepenny\CRUD::getMessage();
                $model['rowProcedure'] = \Threepenny\CRUD::readOne('Procedure', $rowStep['ProcedureId']);
                $model['ActionList'] = \Threepenny\CRUD::readAll('Action', 'Code,Name',
                    'Code,Name,Id');
                $model['roleDetail'] = \Threepenny\CRUD::getRole($stepId, null);
                return $this->view($model);
            } else {
                header("Location:/Step/index");
                return false; // just not to get a warning
            }
        } else {
            $model['message'] = 'Geen toegang. Meld je aan met de juiste gebruikersnaam en paswoord';
            return $this->view($model, 'Views/Vos/LoggingIn.php');
        }
    }

    public function updateOne()
    {
        if (\Threepenny\Identity::isInRole('Admin')) {
            $model['user-name'] = \Threepenny\Identity::get('Name');
            $model = array(
                'tableName' => 'Step',
                'error' => 'Geen'
            );
            $data = array(
                "Name" => filter_input(INPUT_POST, 'Step-Name', FILTER_SANITIZE_STRING),
                "Description" => filter_input(INPUT_POST, 'Step-Description', FILTER_SANITIZE_STRING),
                "ActionId" => filter_input(INPUT_POST, 'Step-ActionId', FILTER_SANITIZE_NUMBER_INT),
                "ProcedureId" => filter_input(INPUT_POST, 'Step-ProcedureId', FILTER_SANITIZE_NUMBER_INT),
                "Order" => filter_input(INPUT_POST, 'Step-Order', FILTER_SANITIZE_NUMBER_INT),
                "Data" => filter_input(INPUT_POST, 'Step-Data', FILTER_SANITIZE_STRING),
                "Id" => filter_input(INPUT_POST, 'Step-Id', FILTER_SANITIZE_NUMBER_INT),
                "UpdatedOn" => filter_input(INPUT_POST, 'Step-UpdatedOnDate', FILTER_SANITIZE_STRING) .
                    ' ' . filter_input(INPUT_POST, 'Step-UpdatedOnTime', FILTER_SANITIZE_STRING)
            );
            if (\Threepenny\CRUD::update('Step', $data, 'ProcedureId')) {
                $model['message'] = "Rij geüpdated {$data['ProcedureId']} in Step";
            } else {
                $model['message'] = "Oeps er is iets fout gelopen! Kan {$data['ProcedureId']} niet updaten in Step";
                $model['error'] = \Threepenny\CRUD::getMessage();
            }
            $model['rowStep'] = \Threepenny\CRUD::readOne('Step', $data['Id']);
            $model['message'] = \Threepenny\CRUD::getMessage();
            //return met Views/ProcedureStep/Index/$procedureId.php werkte niet, vandaar met header
            header("Location:/ProcedureStep/Index/{$data['ProcedureId']}");
            return $this->view($model);
        } else {
            $model['message'] = 'Geen toegang. Meld je aan met de juiste gebruikersnaam en paswoord';
            return $this->view($model, 'Views/Vos/LoggingIn.php');
        }
    }

    public function updatingOne($stepId)
    {
        if (\Threepenny\Identity::isInRole('Admin')) {
            $rowStep = \Threepenny\CRUD::readOne('Step', $stepId);
            $stepColumnsToShow = array('Name', 'Id', 'Order', 'ProcedureId');
            if ($rowStep) {
                $model['rowStep'] = $rowStep;
                $model['listStep'] = \Threepenny\CRUD::readAllWhere('Step', $rowStep['ProcedureId'],
                    'ProcedureId', 'Order, Name', $stepColumnsToShow);
                $model['message'] = \Threepenny\CRUD::getMessage();
                $model['rowProcedure'] = \Threepenny\CRUD::readOne('Procedure', $rowStep['ProcedureId']);
                $model['ActionList'] = \Threepenny\CRUD::readAll('Action', 'Code,Name',
                    'Code,Name,Id');
                $model['roleDetail'] = \Threepenny\CRUD::getRole($stepId, null);
                return $this->view($model);
            } else {
                header("Location:/Step/index");
                return false; // just not to get a warning
            }
        } else {
            $model['message'] = 'Geen toegang. Meld je aan met de juiste gebruikersnaam en paswoord';
            return $this->view($model, 'Views/Vos/LoggingIn.php');
        }
    }
}
