<!--  CreatingOne View for Action entity
 modernways.be
 created by 3penny
 Entreprise de modes et de manières modernes
 created on Tuesday 11th of May 2021 04:31:07 PM
 file name Views/Action/CreatingOne.php/CreatingOne.php
-->
<?php include('Views/Vos/PageHeader.php');?>
<main class="show-room entity">
	<form class="detail" id="form" action="/Action/createOne" method="post">
		<header>
			<h2 class="banner">Creating One Action</h2>
			<nav class="command-panel">
				<button type="submit" value="createOne" name="createOne" class="tile">
					<span class="icon-floppy-disk"></span>
					<span class="screen-reader-text">Create One</span>
				</button>
				<a href="/Action/Index" class="tile">
					<span class="icon-cross"></span>
					<span class="screen-reader-text">Annuleren</span>
				</a>
			</nav>
		</header>
		<fieldset>
			<div class="field">
				<label for="Action-Code">Type *</label>
				<input id="Action-Code" name="Action-Code" class="text" style="width: 20%;" type="text" value=""  requierd />

			</div>
			<div class="field">
				<label for="Action-Name">Naam *</label>
				<input id="Action-Name" name="Action-Name" class="text" style="width: 40%;" type="text" value="" required  />

			</div>
			<div class="field date">
				<label for="Action-UpdatedOnDate" style="width: 20%">Laatst gewijzigd op *</label>
				<input id="Action-UpdatedOnDate" value="<?php echo date('Y-m-d');?>" type="date" name="Action-UpdatedOnDate"   required />
				<label for="Action-UpdatedOnTime" style="width: 5%">om</label>
				<input id="Action-UpdatedOnTime"  value="<?php echo date('H:i:s');?>" type="time" name="Action-UpdatedOnTime"   required />

			</div>
		</fieldset>
		<footer class="feedback">
			<p><?php echo $model['message']; ?></p>
			<p><?php echo isset($model['error']) ? $model['error'] : '';?></p>
		</footer>
	</form>
	<?php include('ReadingAll.php'); ?>
</main>
<?php include('Views/Vos/PageFooter.php');?>
