<!--  UpdatingOne View for Function entity
 modernways.be
 created by 3penny
 Entreprise de modes et de manières modernes
 created on Tuesday 11th of May 2021 04:31:07 PM
 file name Views/Function/UpdatingOne.php/UpdatingOne.php
-->
<?php include('Views/Vos/PageHeader.php');?>
<main class="show-room entity">
	<form class="detail" id="form" action="/Function/UpdateOne" method="post">
		<header>
			<h2 class="banner">Updating One Function</h2>
			<nav class="command-panel">
				<button type="submit" value="updateOne" name="updateOne" class="tile">
					<span class="icon-floppy-disk"></span>
					<span class="screen-reader-text">Update One</span>
				</button>
				<a href="/Function/ReadingOne/<?php echo $model['row']['Id'];?>" class="tile">
					<span class="icon-cross"></span>
					<span class="screen-reader-text">Annuleren</span>
				</a>
			</nav>
		</header>
		<fieldset>
			<div class="field">
				<label for="Function-Name">Naam *</label>
				<input id="Function-Name" name="Function-Name" class="text" style="width: 80%;" type="text" value="<?php echo $model['row']['Name'];?>" required  />
			</div>
			<div class="field">
				<input id="Function-Id" name="Function-Id" style="width: 6em;" type="hidden" value="<?php echo $model['row']['Id'];?>" required  />
			</div>
			<div class="field date">
				<label for="Function-UpdatedOnDate" style="width: 20%">Laatst gewijzigd op *</label>
				<input id="Function-UpdatedOnDate" value="<?php echo date('Y-m-d', strtotime($model['row']['UpdatedOn']));?>" type="date" name="Function-UpdatedOnDate"   required />
                <label for="Action-UpdatedOnTime" style="width: 5%">om</label>
                <input id="Function-UpdatedOnTime"  value="<?php echo date('H:i:s', strtotime($model['row']['UpdatedOn']));?>" type="time" name="Function-UpdatedOnTime"   required />
			</div>
		</fieldset>
		<footer class="feedback">
			<p><?php echo $model['message']; ?></p>
			<p><?php echo isset($model['error']) ? $model['error'] : '';?></p>
		</footer>
	</form>
	<?php include('ReadingAll.php'); ?>
</main>
<?php include('Views/Vos/PageFooter.php');?>
