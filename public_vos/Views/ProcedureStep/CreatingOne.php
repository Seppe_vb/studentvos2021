<!--  CreatingOne View for Step entity
 modernways.be
 created by 3penny
 Entreprise de modes et de manières modernes
 created on Tuesday 11th of May 2021 04:31:07 PM
 file name Views/Step/CreatingOne.php/CreatingOne.php
-->
<?php include('Views/Vos/PageHeader.php');?>
<main class="show-room entity">
    <form class="detail" id="form" action="/ProcedureStep/createOne"  method="post">
        <header>
            <h2 class="banner">Creating One ProcedureStep</h2>

            <nav class="command-panel">
                <button type="submit" value="createOne" name="createOne" class="tile">
                    <span class="icon-floppy-disk"></span>
                    <span class="screen-reader-text">Create One</span>
                </button>
                <a href="/ProcedureStep/Index/<?php echo $model['rowProcedure']['Id'];?>" class="tile">
                    <span class="icon-cross"></span>
                    <span class="screen-reader-text">Annuleren</span>
                </a>
            </nav>
        </header>
        <fieldset>
            <div class="field-procedure">
                <div>
                    <span>Code: </span>
                    <span><?php echo $model['rowProcedure']['Code'];?></span>
                </div>
                <div>
                    <span>Naam: </span>
                    <span><?php echo $model['rowProcedure']['Name'];?></span>
                </div>
                <div>
                    <span>Omschrijving: </span>
                    <span><?php echo $model['rowProcedure']['Description'];?></span>
                </div>
                <div>
                    <span>Rol: </span>
                    <span><?php echo $model['roleDetail']['Code']," ", $model['roleDetail']['Name'];?></span>
                </div>
                <div>
                    <span>Laatst gewijzigd: </span>
                    <span><?php echo date('d/m/Y', strtotime($model['rowProcedure']['UpdatedOn']));?></span>
                    <span> om </span>
                    <span><?php echo date('H:i:s', strtotime($model['rowProcedure']['UpdatedOn']));?></span>
                </div>

        </fieldset>

        <fieldset>
            <div class="field">
                <!--label for="Step-ProcedureId">Procedure</label-->
                <input id="Step-ProcedureId" name="Step-ProcedureId" class="text" style="width: 80%;" type="hidden" value="<?php echo $model['rowProcedure']['Id'];?>"   />
            </div>
            <div class="field">
                <label for="Step-Name">Naam *</label>
                <input id="Step-Name" name="Step-Name" class="text" style="width: 80%;" type="text" value="" required  />
                
            </div>
            <div class="field">
                <label for="Step-Description">Omschrijving</label>
                <input id="Step-Description" name="Step-Description" class="text" style="width: 80%;" type="text" value=""   />
            </div>
            <div class="field">
                <label for="Step-ActionId">Actie *</label>
                <select id="Step-ActionId" name="Step-ActionId" required >
                    <?php
                    if (count($model['ActionList']) > 0)
                    {
                    $i = 1;
                    foreach ($model['ActionList'] as $item)
                    {
                    ?>
                    <option value="<?php echo $item['Id'];?>" <?php echo ($i++ == 1 ? ' selected' : '');?>>
                        <?php echo $item['Code'] . ' ' . $item['Name'];?>
                        <?php
                        }
                        }
                        ?>
                </select>
                
            </div>

            <div class="field">
                <label for="Step-Order">Volgorde</label>
                <input id="Step-Order" name="Step-Order" style="width: 6em;" type="text" value=""   />
            </div>
            <div class="field">
                <label for="Step-Data">Gegevens *</label>
                <textarea id="Step-Data" name="Step-Data" required ></textarea>
                
            </div>
            <div class="field date">
                <label for="Action-UpdatedOnDate" style="width: 20%">Laatst gewijzigd op *</label>
                <input id="Step-UpdatedOnDate" value="<?php echo date('Y-m-d');?>" type="date" name="Step-UpdatedOnDate"   required />
                <label for="Action-UpdatedOnTime" style="width: 5%">om</label>
                <input id="Step-UpdatedOnTime"  value="<?php echo date('H:i:s');?>" type="time" name="Step-UpdatedOnTime"   required />
            </div>
        </fieldset>
        <footer class="feedback">
            <p><?php echo $model['message']; ?></p>
            <p><?php echo isset($model['error']) ? $model['error'] : '';?></p>
        </footer>
    </form>
    <?php include('ReadingAll.php'); ?>
</main>
<?php include('Views/Vos/PageFooter.php');?>
