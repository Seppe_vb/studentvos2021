<!--  ReadingAll view for Procedure entity
 modernways.be
 created by 3penny
 Entreprise de modes et de manières modernes
 created on Tuesday 11th of May 2021 04:31:07 PM
 file name Views/Procedure/ReadingAll.php/ReadingAll.php
-->
<aside class="list">
    <?php
    if (isset($model['listStep']))
    {
    ?>
    <table>
        <th>
        </th>
        <th>
            Volgorde
        </th>
        <th>
            Naam
        </th>
        <th>
            Procedure Id
        </th>
        <th>
            Stap Id
        </th>
        <?php
        foreach ($model['listStep'] as $item) {
            ?>
            <tr>
                <td>
                    <a class="tile" href="/ProcedureStep/readingOne/<?php echo $item['Id']; ?>">
                        <span class="icon-arrow-right"></span>
                        <span class="screen-reader-text">Select</span></a>
                </td>
                <td>
                    <?php echo $item['Order']; ?>
                </td>
                <td>
                    <?php echo $item['Name']; ?>
                </td>
                <td>
                    <?php echo $item['ProcedureId']; ?>
                </td>
                <td>
                    <?php echo $item['Id']; ?>
                </td>
            </tr>
        <?php
        }
        ?>
    </table>
    <?php
    }
    ?>
</aside>
