<?php include('Views/Vos/PageHeader.php');?>
<main class="show-room entity">
    <section class="detail" id="form" action="/ProcedureStep/createOne" method="post">
        <header>
            <h2 class="banner">Index ProcedureStep</h2>
            <nav class="command-panel">

            <a href="/ProcedureStep/CreatingOne/<?php echo $model['rowProcedure']['Id'];?>" class="tile">
                <span class="icon-plus"></span>
                <span class="screen-reader-text">Creating One</span>
            </a>

            <a href="/Procedure/Index" class="tile">
                <span class="icon-cross"></span>
                <span class="screen-reader-text">Terug naar Procedure Index</span>
            </a>
            </nav>
        </header>
        <fieldset>
            <div class="field-procedure">
                <div>
                    <span>Code: </span>
                    <span><?php echo $model['rowProcedure']['Code'];?></span>
                </div>
                <div>
                    <span>Naam: </span>
                    <span><?php echo $model['rowProcedure']['Name'];?></span>
                </div>
                <div>
                    <span>Omschrijving: </span>
                    <span><?php echo $model['rowProcedure']['Description'];?></span>
                </div>
                <div>
                    <span>Rol: </span>
                    <span><?php echo $model['roleDetail']['Code']," ", $model['roleDetail']['Name'];?></span>
                </div>
                <div>
                    <span>Laatst gewijzigd: </span>
                    <span><?php echo date('d/m/Y', strtotime($model['rowProcedure']['UpdatedOn']));?></span>
                    <span> om </span>
                    <span><?php echo date('H:i:s', strtotime($model['rowProcedure']['UpdatedOn']));?></span>
                </div>

        </fieldset>

        <footer class="feedback">
            <p><?php echo $model['message']; ?></p>
            <p><?php echo isset($model['error']) ? $model['error'] : '';?></p>
        </footer>
    </section>
    <?php include('ReadingAll.php'); ?>
</main>
<?php include('Views/Vos/PageFooter.php');?>

