<!--  Index view for App entities
 modernways.be
 created by 3penny
 Entreprise de modes et de manières modernes
 created on Tuesday 11th of May 2021 04:31:07 PM
 file name Vos/Index.php
-->
<?php include('Views/Vos/PageHeader.php');?>
<main class="show-room index">
    <a href="/Organisation/index" class="tile"><span>Organisation</span></a>
    <a href="/User/index" class="tile"><span>User</span></a>
    <a href="/Function/index" class="tile"><span>Function</span></a>
    <a href="/Person/index" class="tile"><span>Person</span></a>
    <a href="/Procedure/index" class="tile"><span>Procedure</span></a>
    <a href="/Role/index" class="tile"><span>Role</span></a>
    <a href="/Step/index" class="tile"><span>Step</span></a>
    <a href="/Action/index" class="tile"><span>Action</span></a>
    <a href="/Log/index" class="tile"><span>Log</span></a>

    <footer class="feedback">
        <p><?php echo isset($model['message']) ? $model['message'] : ''; ?></p>
        <p><?php echo isset($model['error']) ? $model['error'] : '';?></p>
    </footer>
</main>



<?php include('Views/Vos/PageFooter.php');?>
