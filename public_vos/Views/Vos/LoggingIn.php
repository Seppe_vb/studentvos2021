<!--  LoggingIn view for Vos app
 modernways.be
 created by 3penny
 Entreprise de modes et de manières modernes
 created on Tuesday 11th of May 2021 04:31:07 PM
 file name Views/Vos/LoggingIn.php
-->
<?php include('Views/Vos/PageHeader.php');?>
<main class="show-room entity">
	<header>
		<h2 class="banner">Vos</h2>
	</header>	<form class="logging-in" action="" method="POST" id="editor-form" name="editor-form">
		<fieldset>
			<div class="field">
				<label for="User-Name">Gebruikersnaam: </label>
				<input id="User-Name" name="User-Name" class="text" type="text" required/>
			</div>
			<div class="field">
				<label for="User-Password">Paswoord: </label>
				<input id="User-Password" name="User-Password" type="password" required/>
			</div>
			<button type="submit" name="login" value="" formaction="/Vos/login">Aanmelden</button>
		<footer class="feedback">
			<p><?php echo $model['message']; ?></p>
			<p><?php echo isset($model['error']) ? $model['error'] : '';?></p>
		</footer>
		</fieldset>
	</form>
</main>
<?php include('Views/Vos/PageFooter.php');?>
