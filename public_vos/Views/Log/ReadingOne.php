<!--  ReadingOne View for Log entity
 modernways.be
 created by 3penny
 Entreprise de modes et de manières modernes
 created on Tuesday 11th of May 2021 04:31:07 PM
 file name Views/Log/ReadingOne.php/ReadingOne.php
-->
<?php include('Views/Vos/PageHeader.php');?>
<main class="show-room entity">
	<section class="detail" id="form" action="/Log/createOne" method="post">
		<header>
			<h2 class="banner">Reading One Log</h2>
			<nav class="command-panel">
				<a href="/Log/UpdatingOne/<?php echo $model['row']['Id'];?>" class="tile">
					<span class="icon-pencil"></span>
					<span class="screen-reader-text">Updating One</span>
				</a>
				<a href="/Log/CreatingOne" class="tile">
					<span class="icon-plus"></span>
					<span class="screen-reader-text">Creating One</span>
				</a>
				<a href="/Log/DeleteOne/<?php echo $model['row']['Id'];?>" class="tile">
					<span class="icon-bin"></span>
					<span class="screen-reader-text">Delete One</span>
				</a>
				<a href="/Log/Index" class="tile">
					<span class="icon-cross"></span>
					<span class="screen-reader-text">Annuleren</span>
				</a>
			</nav>
		</header>
		<fieldset>
			<div class="field">
				<label for="Log-UserName">Gebruikernaam</label>
				<input id="Log-UserName" name="Log-UserName" class="text" style="width: 12.5%;" type="text" value="<?php echo $model['row']['UserName'];?>"  disabled />
			</div>
			<div class="field">
				<label for="Log-Email">Email</label>
				<input id="Log-Email" name="Log-Email" style="width: 80%;" type="email" value="<?php echo $model['row']['Email'];?>"  disabled />
			</div>
			<div class="field">
				<label for="Log-Role">Rol</label>
				<input id="Log-Role" name="Log-Role" class="text" style="width: 12.5%;" type="text" value="<?php echo $model['row']['Role'];?>"  disabled />
			</div>
			<div class="field">
				<label for="Log-ProcedureCode">Procedure code</label>
				<input id="Log-ProcedureCode" name="Log-ProcedureCode" class="text" style="width: 6.25%;" type="text" value="<?php echo $model['row']['ProcedureCode'];?>"  disabled />
			</div>
			<div class="field">
				<label for="Log-ProcedureTitle">Procedure titel</label>
				<input id="Log-ProcedureTitle" name="Log-ProcedureTitle" class="text" style="width: 80%;" type="text" value="<?php echo $model['row']['ProcedureTitle'];?>"  disabled />
			</div>
			<div class="field">
				<label for="Log-StepTitle">Step titel</label>
				<input id="Log-StepTitle" name="Log-StepTitle" class="text" style="width: 80%;" type="text" value="<?php echo $model['row']['StepTitle'];?>"  disabled />
			</div>
			<div class="field">
				<label for="Log-ActionCode">Actie code</label>
				<input id="Log-ActionCode" name="Log-ActionCode" class="text" style="width: 2.5%;" type="text" value="<?php echo $model['row']['ActionCode'];?>"  disabled />
			</div>
			<div class="field">
				<label for="Log-CallNumber">Oproepnummer</label>
				<input id="Log-CallNumber" name="Log-CallNumber" class="text" style="width: 6.25%;" type="text" value="<?php echo $model['row']['CallNumber'];?>"  disabled />
			</div>
			<div class="field">
				<label for="Log-SendNumber">Opgeroepen nummer</label>
				<input id="Log-SendNumber" name="Log-SendNumber" class="text" style="width: 6.25%;" type="text" value="<?php echo $model['row']['SendNumber'];?>"  disabled />
			</div>
			<div class="field">
				<input id="Log-Id" name="Log-Id" style="width: 6em;" type="hidden" value="<?php echo $model['row']['Id'];?>"   />
			</div>
		</fieldset>
		<footer class="feedback">
			<p><?php echo $model['message']; ?></p>
			<p><?php echo isset($model['error']) ? $model['error'] : '';?></p>
		</footer>
	</section>
	<?php include('ReadingAll.php'); ?>
</main>
<?php include('Views/Vos/PageFooter.php');?>
