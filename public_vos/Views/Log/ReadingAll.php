<!--  ReadingAll view for Log entity
 modernways.be
 created by 3penny
 Entreprise de modes et de manières modernes
 created on Tuesday 11th of May 2021 04:31:07 PM
 file name Views/Log/ReadingAll.php/ReadingAll.php
-->
<aside class="list">
	<?php
		if ($model['list'])
		{
	?>
	<table>
		<?php
			foreach ($model['list'] as $item)
			{
		?>
		<tr>
			<td>
				<a class="tile"
				href="/Log/readingOne/<?php echo $item['Id'];?>">
				<span class="icon-arrow-right"></span>
				<span class="screen-reader-text">Select</span></a>
			</td>
			<td>
				<?php echo $item['UserName'];?>
			</td>
			<td>
				<?php echo $item['Role'];?>
			</td>
			<td>
				<?php echo $item['ProcedureTitle'];?>
			</td>
			<td>
				<?php echo $item['StepTitle'];?>
			</td>
			<td>
				<?php echo $item['ActionCode'];?>
			</td>

		</tr>
		<?php
		}
		?>
	</table>
	<?php
		}
		else
		{
	?>
	<p>Geen Logboek</p>
	<?php
	}
	?>
</aside>

